/**
 * This file will automatically be loaded by webpack and run in the "renderer" context.
 * To learn more about the differences between the "main" and the "renderer" context in
 * Electron, visit:
 *
 * https://electronjs.org/docs/latest/tutorial/process-model
 *
 * By default, Node.js integration in this file is disabled. When enabling Node.js integration
 * in a renderer process, please be aware of potential security implications. You can read
 * more about security risks here:
 *
 * https://electronjs.org/docs/tutorial/security
 *
 * To enable Node.js integration in this file, open up `main.js` and enable the `nodeIntegration`
 * flag:
 *
 * ```
 *  // Create the browser window.
 *  mainWindow = new BrowserWindow({
 *    width: 800,
 *    height: 600,
 *    webPreferences: {
 *      nodeIntegration: true
 *    }
 *  });
 * ```
 */

import './index.css';

render("/");

async function render(pathToGo:string) {
    const data = await window.electronApi.getFiles(pathToGo); // Index.ts :: data = [directory_name, filenames] 
    const main = document.querySelector('main');

    main.appendChild(renderFiles(data, pathToGo));
}

function renderFiles(data:string[], actualPath:string):HTMLElement {
    const mainDiv = document.createElement('div')
    // create uan header with a container class
    const divContainerHeader = document.createElement('header');
    divContainerHeader.className = "container containerCenter";
    // create elements (buttons and div), children of header
    const previousButton = document.createElement('button');
    previousButton.className = "previousButton";
    previousButton.textContent = '<';
    const urlDiv = document.createElement('div');
    urlDiv.className = "pathDisplay";
    urlDiv.textContent = 'PATH DIV';
    // appendchild
    divContainerHeader.appendChild(previousButton);
    divContainerHeader.appendChild(urlDiv);

    const divContainerBody = document.createElement('body') ;
    divContainerBody.className = "container";
    console.log(data)

    for(let i = 0; i < data.length; i++){
        const divItem = document.createElement('div')
        divItem.className = "divItem"

        const imgIcon = document.createElement('img')
        imgIcon.src = "https://upload.wikimedia.org/wikipedia/commons/5/59/OneDrive_Folder_Icon.svg";
        imgIcon.id = "img"+i
        const divFormInside = document.createElement('div')
        divFormInside.className = "item";
        divFormInside.textContent = data[i];
        const id = "id"+i
        divItem.id = id
        // const linker = document.querySelector("#"+id);
        // linker.addEventListener('click', () => {
        //     render(actualPath + data[i] + "\\")
        //     //If folder
        //     // Supprimer tout ce que contient la balise main + relancer render de 
        //     // renderFiles
        //     //If file
        // })
        divItem.append(imgIcon, divFormInside);
        divContainerBody.appendChild(divItem);
    }

    mainDiv.appendChild(divContainerHeader);
    mainDiv.appendChild(divContainerBody);

    return mainDiv;
}