
export interface IElectronAPI {
  getFiles: (pathToGo:string) => Promise<string[]>,
}
declare global {
  interface Window {
    electronApi: IElectronAPI
  }
}